# Ruby starter server

A nice little server, written with ruby!

## Setup

These are instructions for setting up this project on macOS.

### Homebrew

First, make sure you have [brew](https://brew.sh). You can check by opening a terminal and running

```
$ brew --version
```

if you get a version number, and no error messages about "brew not found in PATH", then you have brew. otherwise visit https://brew.sh to get it.

### Ruby

Install ruby with

```
$ brew install ruby
```

Then install the ruby gems for this project with

```
$ bundle install
```

Now, you are ready to run this server!

## Running

All you need to do is run

```
$ ruby server.rb
```

and then open a web browser to [localhost:4567](localhost:4567)

you can quit the server by pressing ctrl+c in the same terminal you ran it from.
